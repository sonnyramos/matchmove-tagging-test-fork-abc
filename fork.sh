#!/usr/bin/env bash

REPO_INFO=${1}
IFS=: read -r FORKED_REPO MASTER_REPO <<< "$REPO_INFO"

rm -rf ${APPDIR}/repo
echo ">>>>>>>> Checking out: https://bitbucket.org/${FORKED_REPO}.git"
git clone https://bitbucket.org/${FORKED_REPO}.git ${APPDIR}/repo
cd ${APPDIR}/repo/
echo ">>>>>>>> Setting upstream repo to : ${MASTER_REPO}"
git remote add upstream https://bitbucket.org/${MASTER_REPO}.git
git fetch upstream

echo ">>>>>>>> Getting updates from master repo..."
git pull upstream master

echo ">>>>>>>> Synching codes from master repo..."
git commit . -m"Synching codes from master repo ${MASTER_REPO}"
git push origin master
rm -rf ${APPDIR}/repo
